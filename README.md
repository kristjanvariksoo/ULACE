# ULACE

ULACE (User Level Access Controlled Excel)

## Probleem

### KV probleem
Hunnikus ajateenijaid, igal ajateenijal on hunnik omadusi, mida tahaks hallata. Näiteks: isikukood, ligipääsu kaardi nr, elukoht, spordi tulemused, varustuse suurused, varasema teenistuse andmed, tuleva teenistuse andmed.  
*Praegune probleemne olukord:* Ei saanud ÜFT-i läbi ja ei saa enam väljaloale? Noh aga muuda lihtsalt ära oma tulemus.

### BEST probleem
Ürituste korraldamine ja registreerimise jaoks on excel, kus on inimesed ja iga inimese kohta, et kas nad on ära maksnud ja kas nad tulevad oma autoga jne.  
*Praegune probleemne olukord:* Kõik kohad täis? Kustuta keegi teine eest ära ja pane ennast asemele.

### Doodle probleem
Mentorite registreerimine doodlega, et mis tiim läheb mis mentori juurde.  
*Praegune probleemne olukord:* Meeldiv mentor ära võeldud? Noh võta teine tiim maha ja pane ennast asemele.

## Idee
Excel/Sheets aga spetsiifilisema access controlliga - ainult osasid lahtreid saavad inimesed muuta. Võimalik ära keelata teiste andmete muutmine ja isegi nägemine. Superadminidel on ligipääs kõikide andmetele nii muutmiseks kui nägemiseks.

## Teostuse ideed
### GIT
Kõigil on oma giti repo kus YAML või sarnases on andmed ja siis mingi CI/CD automaatika käib sealt kokku mätsimas master faili ja sealt ka uuesti laiali.

### EtherCalc
Small web app that runs alongside a self-hosted ethercalc (https://ethercalc.net/ - essentially self hosted google sheets with nice API support). That web-app creates for each table actually multiple and keep them all constantly in sync. Essentially for every master table it creates as many peoples data as there is worth of seperate tables and shows only necessary data.

### WebApp
A webapp that interfaces with a single google sheets or ethercalc sheet essentially providing the UI through which people view/edit it entirely.

## Use cases

Just adding/registering but not removing with everyone having an overview, but only admin being able to remove - essentially google forms that outputs to sheets, but that already exists. Pretty rare that people remove themselves.

## Authentication

Make it really easy to link with Facebook, Google, Github, LDAP authentication. 
